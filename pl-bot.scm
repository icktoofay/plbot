(library (pl-bot)
  (export run-pl-bot)
  (import (rnrs)
          (bot-api)
          (json)
          (piglatin))

  (define process-message
    (lambda (api-instance message)
      (let ([text (object-deref "text" message)]
            [chat-id (object-deref "id" (object-deref "chat" message))])
        (put-datum (current-output-port) text) (newline)
        (send-message api-instance chat-id (phrase->piglatin text)))))

  (define process-update
    (lambda (api-instance update)
      (let ([message (object-deref "message" update)])
        (when message (process-message api-instance message)))))

  (define run-pl-bot
    (lambda (api-instance offset)
      (let ([updates (get-updates api-instance offset)])
        (let loop ([updates (vector->list updates)] [offset offset])
          (if (null? updates)
              (run-pl-bot api-instance offset)
              (let* ([update (car updates)]
                     [update-id (object-deref "update_id" update)])
                (process-update api-instance update)
                (loop (cdr updates) (+ update-id 1)))))))))
