(library (json)
  (export parse make-object object? object-assocs object-deref)
  (import (rnrs))

  (define-record-type object
    (sealed #t)
    (nongenerative object-8d032ba3-39a3-4500-9245-8525806c096e)
    (fields assocs))

  (define object-deref
    (lambda (key object)
      (let ([pair (assoc key (object-assocs object))])
        (if pair (cdr pair) #f))))

  (define skip-ws
    (lambda (s pos)
      (if (and (< pos (string-length s))
               (char-whitespace? (string-ref s pos)))
          (skip-ws s (+ pos 1))
          pos)))

  (define string=-at?
    (lambda (haystack pos needle)
      (and (>= (string-length haystack) (+ pos (string-length needle)))
           (string=? (substring haystack pos (+ pos (string-length needle)))
                     needle))))

  (define hex-char->integer
    (lambda (ch)
      (case ch
        [(#\0) 0] [(#\1) 1] [(#\2) 2] [(#\3) 3] [(#\4) 4]
        [(#\5) 5] [(#\6) 6] [(#\7) 7] [(#\8) 8] [(#\9) 9]
        [(#\A #\a) #xA] [(#\B #\b) #xB] [(#\C #\c) #xC]
        [(#\D #\d) #xD] [(#\E #\e) #xE] [(#\F #\f) #xF]
        [else (error 'hex-char->integer "invalid hex char" ch)])))

  (define hex-string->integer
    (lambda (s)
      (let loop ([acc 0] [pos 0])
        (if (= pos (string-length s))
            acc
            (loop (+ (* acc #x10) (hex-char->integer (string-ref s pos)))
                  (+ pos 1))))))

  (define parse-from
    (lambda (s pos)
      (let ([pos (skip-ws s pos)])
        (unless (< pos (string-length s))
          (error 'parse-from "premature EOF" s))
        (case (string-ref s pos)
          [(#\{) (parse-object-from s pos)]
          [(#\[) (parse-array-from s pos)]
          [(#\") (parse-string-from s pos)]
          [(#\t) (if (string=-at? s pos "true")
                     (values #t (+ pos 4))
                     (error 'parse-from "invalid primitive"))]
          [(#\f) (if (string=-at? s pos "false")
                     (values #f (+ pos 5))
                     (error 'parse-from "invalid primitive"))]
          [(#\n) (if (string=-at? s pos "null")
                     (values '() (+ pos 4))
                     (error 'parse-from "invalid primitive"))]
          [(#\- #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)
           (parse-number-from s pos)]
          [else (error 'parse-from "unrecognized character"
                       (string-ref s pos))]))))

  (define parse-object-from
    (lambda (s pos)
      (parse-comma-delimited s (+ pos 1) parse-pair-from make-object #\})))

  (define parse-array-from
    (lambda (s pos)
      (parse-comma-delimited s (+ pos 1) parse-from list->vector #\])))

  (define parse-pair-from
    (lambda (s pos)
      (let*-values ([(key pos) (parse-string-from s pos)]
                    [(pos) (skip-ws s pos)])
        (unless (and (< pos (string-length s))
                     (char=? (string-ref s pos) #\:))
          (error 'parse-pair-from "expected colon" s))
        (let-values ([(value pos) (parse-from s (+ pos 1))])
          (values (cons key value) pos)))))

  (define parse-comma-delimited
    (lambda (s pos parse-item make term)
      (let ([pos (skip-ws s pos)])
        (if (and (< pos (string-length s))
                 (char=? (string-ref s pos) term))
            (values (make '()) (+ pos 1))
            (let-values ([(item pos) (parse-item s pos)])
              (let loop ([items (list item)] [pos pos])
                (let ([pos (skip-ws s pos)])
                  (cond
                    [(= pos (string-length s))
                     (error 'parse-comma-delimited "premature EOF" s)]
                    [(char=? (string-ref s pos) term)
                     (values (make (reverse items)) (+ pos 1))]
                    [(char=? (string-ref s pos) #\,)
                     (let-values ([(item pos) (parse-item s (+ pos 1))])
                       (loop (cons item items) pos))]
                    [else (error 'parse-comma-delimited
                                 "unexpected character" s)]))))))))

  (define utf16-decode
    (lambda (units)
      (let loop ([points '()] [units units])
        (cond
          [(null? units) (list->string (reverse points))]
          [(char? (car units))
           (loop (cons (car units) points) (cdr units))]
          [(and (>= (car units) #xD800)
                (<  (car units) #xDC00)
                (not (null? (cdr units)))
                (>= (cadr units) #xDC00)
                (<  (cadr units) #xE000))
           (loop (cons (integer->char
                         (+ (* #x400 (- (car units) #xD800))
                            (- (cadr units) #xDC00) #x10000)) points)
                 (cddr units))]
          [(and (>= (car units) #xD800)
                (<  (car units) #xE000))
           (error 'utf16-decode "unpaired surrogate" (car units))]
          [else (loop (cons (integer->char (car units)) points)
                      (cdr units))]))))

  (define parse-string-from
    (lambda (s pos)
      (let ([pos (skip-ws s pos)])
        (unless (and (< pos (string-length s))
                     (char=? (string-ref s pos) #\"))
            (error 'parse-string-from "no starting quote" s))
        (let loop ([chars '()] [pos (+ pos 1)])
          (when (= pos (string-length s))
            (error 'parse-string-from "EOF in string" s))
          (case (string-ref s pos)
            [(#\") (values (utf16-decode (reverse chars)) (+ pos 1))]
            [(#\\) (let-values ([(ch pos) (parse-escape s (+ pos 1))])
                     (loop (cons ch chars) pos))]
            [else (loop (cons (string-ref s pos) chars) (+ pos 1))])))))

  (define parse-escape
    (lambda (s pos)
      (unless (< pos (string-length s))
        (error 'parse-escape "EOF in escape" s))
      (case (string-ref s pos)
        [(#\" #\\ #\/) (values (string-ref s pos) (+ pos 1))]
        [(#\b) (values #\backspace (+ pos 1))]
        [(#\f) (values #\page      (+ pos 1))]
        [(#\n) (values #\newline   (+ pos 1))]
        [(#\r) (values #\return    (+ pos 1))]
        [(#\t) (values #\tab       (+ pos 1))]
        [(#\u) (if (< (+ pos 4) (string-length s))
                   (values (hex-string->integer
                             (substring s (+ pos 1) (+ pos 5)))
                           (+ pos 5))
                   (error 'parse-escape "EOF in escape" s))]
        [else (error 'parse-escape "bad string escape" s)])))

  (define parse-number-from
    (lambda (s start)
      (let loop ([pos start])
        (if (and (< pos (string-length s))
                 (memv (string-ref s pos)
                       '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9
                         #\. #\- #\+ #\e #\E)))
            (loop (+ pos 1))
            (values (string->number (substring s start pos)) pos)))))

  (define parse
    (lambda (s)
      (let*-values ([(item pos) (parse-from s 0)]
                    [(pos) (skip-ws s pos)])
        (if (= pos (string-length s)) item
            (error 'parse "junk after end" s))))))
