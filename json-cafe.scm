#! /usr/bin/env scheme-script
(import (chezscheme)
        (json))
(parameterize ([waiter-prompt-and-read
                 (lambda (level)
                   (display (waiter-prompt-string) (console-output-port))
                   (write-char #\space (console-output-port))
                   (get-line (console-input-port)))])
  (new-cafe parse))
