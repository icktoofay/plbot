(library (piglatin)
  (export word->piglatin phrase->piglatin)
  (import (rnrs))

  (define vowels '(#\a #\e #\i #\o #\u))

  (define vowel?
    (lambda (ch)
      (memv (char-downcase ch) vowels)))

  (define consonant?
    (lambda (ch)
      (not (vowel? ch))))

  (define punctuation?
    (lambda (ch)
      (not (or (char-alphabetic? ch)
               (char-numeric? ch)))))

  (define word-separator?
    (lambda (ch)
      (or (char-whitespace? ch)
          (memv ch '(#\-)))))

  (define string-empty?
    (lambda (s)
      (zero? (string-length s))))

  (define string-reverse
    (lambda (s)
      (list->string (reverse (string->list s)))))

  (define all
    (lambda (pred l)
      (cond
        [(null? l) #t]
        [(not (pred (car l))) #f]
        [else (all pred (cdr l))])))

  (define span
    (lambda (pred l)
      (let loop ([r '()] [l l])
        (if (or (null? l)
                (not (pred (car l))))
            (values (reverse r) l)
            (loop (cons (car l) r) (cdr l))))))

  (define string-span
    (lambda (pred s)
      (let-values ([(l r) (span pred (string->list s))])
        (values (list->string l) (list->string r)))))

  (define cluster-split
    (lambda (s)
      (let-values ([(c rest) (string-span consonant? s)])
        (if (string-empty? c)
            (let*-values ([(v rest) (string-span vowel? s)]
                          [(c rest) (string-span consonant? rest)])
              (values (string-append v c) rest))
            (values c rest)))))

  (define detect-casing
    (lambda (word)
      (let ([first (string-ref word 0)]
            [rest (substring word 1 (string-length word))])
        (cond
          [(char-lower-case? first) 'lower]
          [(string-empty? rest) 'title]
          [(all char-upper-case? (string->list rest)) 'upper]
          [else 'title]))))

  (define decase
    (lambda (word)
      (let ([first (string-ref word 0)]
            [rest (substring word 1 (string-length word))])
        (string-append (string (char-downcase first)) rest))))

  (define recase
    (lambda (word casing)
      (let ([first (string-ref word 0)]
            [rest (substring word 1 (string-length word))])
        (case casing
          [(lower) (string-append (string first) rest)]
          [(title) (string-append (string (char-upcase first)) rest)]
          [(upper) (string-append (string (char-upcase first))
                                  (string-upcase rest))]
          [else (assertion-violation 'recase "invalid casing" casing)]))))

  (define word->piglatin
    (lambda (word)
      (let-values ([(init rest) (cluster-split word)])
        (recase (string-append rest (decase init) "ay")
                (detect-casing word)))))

  (define over-unpunctuated
    (lambda (func word)
      (let*-values ([(init-punct rest) (string-span punctuation? word)]
                    [(trail-punct rest) (string-span punctuation?
                                                     (string-reverse rest))])
        (string-append init-punct
                       (func (string-reverse rest))
                       (string-reverse trail-punct)))))

  (define punctuated-word->piglatin
    (lambda (word)
      (over-unpunctuated
        (lambda (word)
          (if (string-empty? word) word
              (word->piglatin word)))
        word)))

  (define map-words
    (lambda (func phrase)
      (let loop1 ([i 0] [result ""])
        (cond
          [(= i (string-length phrase)) result]
          [(word-separator? (string-ref phrase i))
           (loop1 (+ i 1) (string-append result
                                         (string (string-ref phrase i))))]
          [else (let loop2 ([j i])
                  (if (or (= j (string-length phrase))
                          (word-separator? (string-ref phrase j)))
                      (let* ([word (substring phrase i j)]
                             [mapped (func word)])
                        (loop1 j (string-append result mapped)))
                      (loop2 (+ j 1))))]))))

  (define phrase->piglatin
    (lambda (english)
      (map-words punctuated-word->piglatin english))))
