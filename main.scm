#! /usr/bin/env scheme-script
(import (rnrs)
        (curl)
        (bot-api)
        (pl-bot))

(define api-key
  (call-with-input-file "api.key" get-datum))

(call-with-curl
  (lambda ()
    (call-with-api-instance api-key
      (lambda (api-instance) (run-pl-bot api-instance #f)))))
