(library (bot-api)
  (export call-with-api-instance api-instance?
          get-updates send-message)
  (import (rnrs)
          (curl)
          (prefix (json) json-))

  (define-record-type api-instance
    (sealed #t) (opaque #t)
    (fields base-url easy-handle (mutable buffer)))

  (define tg-api-prefix "https://api.telegram.org/bot")

  (define bytevector-append
    (lambda bvs
      (u8-list->bytevector (apply append (map bytevector->u8-list bvs)))))

  (define write-function-for-instance
    (lambda (instance)
      (lambda (data)
        (api-instance-buffer-set!
          instance
          (bytevector-append (api-instance-buffer instance)
                             data)))))

  (define call-with-api-instance
    (lambda (api-key f)
      (call-with-easy
        (lambda (easy)
          (let ([instance (make-api-instance
                            (string-append tg-api-prefix api-key "/")
                            easy #f)])
            (easy-write-set! easy (write-function-for-instance instance))
            (f instance))))))

  (define extract-result
    (lambda (who response)
      (if (eqv? (json-object-deref "ok" response) #t)
          (json-object-deref "result" response)
          (error who "Telegram API returned not-OK" response))))

  (define simple-request
    (lambda (api-instance who method fields)
      (let ([base-url (api-instance-base-url api-instance)]
            [easy (api-instance-easy-handle api-instance)])
        (api-instance-buffer-set! api-instance #vu8())
        (easy-url-set! easy (string-append base-url method))
        (easy-mime-post-set! easy (association-list->mime easy fields))
        (easy-perform easy)
        (let ([data (api-instance-buffer api-instance)])
          (api-instance-buffer-set! api-instance #f)
          #;(begin
              (put-datum (current-output-port) (utf8->string data))
              (newline))
          (extract-result who (json-parse (utf8->string data)))))))

  (define get-updates
    (lambda (api-instance offset)
      (let ([params (list (and offset
                               (cons "offset"
                                     (string->utf8 (number->string offset))))
                          (cons "timeout" #vu8(#x33 #x30 #x30)))])
        (simple-request api-instance 'get-updates "getUpdates"
                        (filter values params)))))

  (define send-message
    (lambda (api-instance chat-id text)
      (simple-request api-instance 'send-message "sendMessage"
                      (list (cons "chat_id"
                                  (string->utf8
                                    (if (number? chat-id)
                                        (number->string chat-id) chat-id)))
                            (cons "text" (string->utf8 text)))))))
