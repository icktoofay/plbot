(library (curl)
  (export global-init global-cleanup call-with-curl easy-handle?
          easy-init easy-cleanup call-with-easy
          easy-url-set! easy-write-set! easy-mime-post-set! easy-perform
          mime-init mime? mime-add-part! mime-part?
          mime-part-set-name! mime-part-set-data! association-list->mime)
  (import (chezscheme))

  (define-record-type easy-handle
    (sealed #t) (opaque #t)
    (fields (mutable pointer)
            (mutable write-function)
            (mutable mime-post)))

  (define-record-type mime
    (sealed #t) (opaque #t)
    (fields easy-handle pointer))

  (define-record-type mime-part
    (sealed #t) (opaque #t)
    (fields mime pointer))

  (define mime-guardian (make-guardian))

  (define memcpy->bv #f)
  (define native-global-init #f)
  (define native-global-cleanup #f)
  (define native-easy-strerror #f)
  (define native-easy-init #f)
  (define native-easy-cleanup #f)
  (define native-easy-setopt-iptr #f)
  (define native-easy-setopt-utf8 #f)
  (define native-easy-perform #f)
  (define native-mime-init #f)
  (define native-mime-free #f)
  (define native-mime-add-part #f)
  (define native-mime-name #f)
  (define native-mime-data #f)

  (define CURL_GLOBAL_ALL 3)
  (define CURLOPT_URL 10002)
  (define CURLOPT_MIMEPOST 10269)
  (define CURLOPT_WRITEFUNCTION 20011)

  (define check-result
    (lambda (who result)
      (when (not (zero? result))
        (error who (native-easy-strerror result)))))

  (define global-init
    (lambda ()
      (load-shared-object "libc.so.6")
      (load-shared-object "libcurl.so.4")

      (set! memcpy->bv
        (foreign-procedure "memcpy" (u8* iptr size_t) iptr))
      (set! native-global-init
        (foreign-procedure __collect_safe "curl_global_init" (long) int))
      (set! native-global-cleanup
        (foreign-procedure __collect_safe "curl_global_cleanup" () void))
      (set! native-easy-strerror
        (foreign-procedure __collect_safe "curl_easy_strerror" (int) utf-8))
      (set! native-easy-init
        (foreign-procedure __collect_safe "curl_easy_init" () iptr))
      (set! native-easy-cleanup
        (foreign-procedure __collect_safe "curl_easy_cleanup" (iptr) void))
      (set! native-easy-setopt-iptr
        (foreign-procedure
          __collect_safe "curl_easy_setopt" (iptr int iptr) int))
      (set! native-easy-setopt-utf8
        (foreign-procedure "curl_easy_setopt" (iptr int utf-8) int))
      (set! native-easy-perform
        (foreign-procedure __collect_safe "curl_easy_perform" (iptr) int))
      (set! native-mime-init (foreign-procedure "curl_mime_init" (iptr) iptr))
      (set! native-mime-free (foreign-procedure "curl_mime_free" (iptr) void))
      (set! native-mime-add-part
        (foreign-procedure "curl_mime_addpart" (iptr) iptr))
      (set! native-mime-name
        (foreign-procedure "curl_mime_name" (iptr utf-8) int))
      (set! native-mime-data
        (foreign-procedure "curl_mime_data" (iptr u8* size_t) int))

      (check-result 'global-init (native-global-init CURL_GLOBAL_ALL))))

  (define global-cleanup
    (lambda ()
      (native-global-cleanup)))

  (define call-with-curl
    (lambda (func)
      (global-init)
      (let ([result (func)])
        (global-cleanup)
        result)))

  (define easy-init
    (lambda ()
      (let ([pointer (native-easy-init)])
        (when (zero? pointer)
          (error 'easy-init "failed to create easy handle"))
        (make-easy-handle pointer #f #f))))

  (define easy-cleanup
    (lambda (handle)
      (native-easy-cleanup (easy-handle-pointer handle))
      (easy-handle-pointer-set! handle 0)))

  (define call-with-easy
    (lambda (func)
      (let* ([handle (easy-init)]
             [result (func handle)])
        (easy-cleanup handle)
        result)))

  (define easy-url-set!
    (lambda (handle url)
      (check-result 'easy-url-set!
                    (native-easy-setopt-utf8
                      (easy-handle-pointer handle)
                      CURLOPT_URL url))))

  (define easy-write-set!
    (lambda (handle func)
      (easy-handle-write-function-set! handle func)))

  (define easy-mime-post-set!
    (lambda (handle mime)
      (check-result 'easy-mime-post-set!
                    (native-easy-setopt-iptr
                      (easy-handle-pointer handle)
                      CURLOPT_MIMEPOST
                      (if mime (mime-pointer mime) 0)))
      (easy-handle-mime-post-set! handle mime)))

  (define call-with-locked
    (lambda (object func)
      (dynamic-wind
        (lambda () (lock-object object))
        func
        (lambda () (unlock-object object)))))

  (define address->bytevector
    (lambda (addr len)
      (let ([bv (make-bytevector len)])
        (memcpy->bv bv addr len)
        bv)))

  (define wrap-write-function
    (lambda (write)
      (foreign-callable
        __collect_safe
        (lambda (data size-a size-b file)
          (write (address->bytevector data (* size-a size-b)))
          (* size-a size-b))
        (iptr size_t size_t iptr) size_t)))

  (define call-with-write-function-set
    (lambda (handle func)
      (let ([callable #f])
        (dynamic-wind
          (lambda ()
            (set! callable
              (if (easy-handle-write-function handle)
                  (wrap-write-function (easy-handle-write-function handle))
                  #f))
            (lock-object callable)
            (call-with-locked callable
              (lambda ()
                (unlock-object callable)
                (native-easy-setopt-iptr
                  (easy-handle-pointer handle) CURLOPT_WRITEFUNCTION
                  (if callable (foreign-callable-entry-point callable) 0))
                (lock-object callable))))
          func
          (lambda ()
            (native-easy-setopt-iptr (easy-handle-pointer handle)
                                     CURLOPT_WRITEFUNCTION 0)
            (unlock-object callable)
            (set! callable #f))))))

  (define easy-perform
    (lambda (handle)
      (call-with-write-function-set handle
        (lambda ()
          (check-result 'easy-perform
                        (native-easy-perform
                          (easy-handle-pointer handle)))))))

  (define mime-init
    (lambda (easy)
      (let loop ()
        (let ([pointer (mime-guardian)])
          (when pointer
            (native-mime-free pointer)
            (loop))))
      (let* ([pointer (native-mime-init (easy-handle-pointer easy))]
             [handle (make-mime easy pointer)])
        (mime-guardian handle pointer)
        handle)))

  (define mime-add-part!
    (lambda (mime)
      (make-mime-part mime (native-mime-add-part (mime-pointer mime)))))

  (define mime-part-set-name!
    (lambda (part name)
      (native-mime-name (mime-part-pointer part) name)))

  (define mime-part-set-data!
    (lambda (part data)
      (native-mime-data (mime-part-pointer part)
                        data (bytevector-length data))))

  (define association-list->mime
    (lambda (easy l)
      (let ([mime (mime-init easy)])
        (let loop ([l l])
          (if (null? l) mime
              (let ([part (mime-add-part! mime)])
                (mime-part-set-name! part (car (car l)))
                (mime-part-set-data! part (cdr (car l)))
                (loop (cdr l)))))))))
